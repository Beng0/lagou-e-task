/*
尽可能还原 Promise 中的每一个 API, 并通过注释的方式描述思路和原理.
*/

const PENDING = 'pending';
const FULFILLED = 'fulfilled';
const REJECTED = 'rejected';
class MyPromise {
  // 接收一个立即执行的函数
  constructor(cb) {
    try {
      cb(this.resolve, this.reject)
    } catch (error) {
      this.reject(error);
    }
  }
  // 状态
  status = PENDING;
  // 成功参数
  value = undefined;
  // 失败原因
  reason = undefined;
  // 多个then并行执行时，then成功回调队列
  successCallback = [];
  // 多个then并行执行时，then失败回调队列
  failCallback = [];

  // resolve---将状态从等待转为成功,可接受参数
  resolve = value => {
    if (this.status !== PENDING) return;
    this.status = FULFILLED;
    this.value = value;
    // 只要成功回调队列还有未执行的回调，就从头部截取并执行
    while(this.successCallback.length) this.successCallback.shift()();
  }

  // reject---将状态从等待转为失败,可接受参数
  reject = reason => {
    if (this.status !== PENDING) return;
    this.status = REJECTED;
    this.reason = reason;
    // 只要
    while(this.failCallback.length) this.failCallback.shift()();
  }

  // 判断当前状态，执行相应逻辑，并返回新的MyPromise对象
  then (success, failed) {
    // then回调中可能没有参数，需要补充函数
    success = success ? success : value => value;
    failed = failed ? failed : reason => reason;
    const promise = new MyPromise((nextResolve, nextReject) => {
      const successAction = () => {
        // 避免回调返回了MyPromise自身
        // 需要检测promise和v是否是同一个MyPromise对象
        // 由于会立即执行，访问不到promise变量，此处需要注意采用一个异步的方式获取promise变量
        queueMicrotask(() => {
          try {
            // 成功回调
            const v = success(this.value)
            // 查看回调返回的是个新的MyPromise对象还是基本数据类型数据，是MyPromise对象则查看状态执行相应resolve，reject
            statusCheck(promise, v, nextResolve, nextReject);
          } catch (error) {
            nextReject(error);
          }
        })
      }
      const failAction = () => {
        queueMicrotask(() => {
          try {
            // 失败回调
            const v = failed(this.reason);
            // 查看回调返回的是个新的MyPromise对象还是基本数据类型数据，是MyPromise对象则查看状态执行相应resolve，reject
            // 当reject时，错误向下传递，给下一个then接收
            statusCheck(promise, v, nextResolve, nextReject);
          } catch (error) {
            nextReject(error);
          }
        })
      }
      if (this.status === FULFILLED) { // 当前状态成功
        successAction();
      } else if (this.status === REJECTED) { // 当前状态失败
        failAction();
      } else { // 异步调用导致一直Pending状态
        this.successCallback.push(() => {
          successAction();
        })
        this.failCallback.push(() => {
          failAction();
        })
      }
    })
    return promise;
  }

  catch (error) {
    return new MyPromise((undefined, reject) => {
      reject(error)
    })
  }

  // 无论MyPromise最终状态是什么，回调方法都会执行，finally调用之后可以使用 then获取MyPromise最终状态
  finally (callback) {
    // 先执行指定函数，然后返回一个MyPromise对象
    return this.then(value => {
      return MyPromise.resolve(callback()).then(() => value)
    }, reason => {
      return MyPromise.resolve(callback()).then(() => { throw reason })
    })
  }

  // 原型方法
  // 数组中的依次执行，通过then回调返回
  static all(array) {
    return new MyPromise((resolve, reject) => {
      const result = [];
      let count = 0;
      for (let i = 0; i < array.length; i++ ) {
        // 判断是否为MyPromise对象
        if (array[i] instanceof MyPromise) {
          array[i].then(res => {
            result[i] = res;
          }, error => reject(error));
          count++;
        } else {
          result[i] = array[i];
          count++;
        }
        // 处理异步情况下等全部执行完毕在进行成功回调
        if (count === array.length) {
          resolve(result);
        }
      }
    })
  }

  // 返回MyPromise对象成功状态
  static resolve(value) {
    if (value instanceof MyPromise) {
      return value;
    } else {
      return new MyPromise(resolve => resolve(value))
    }
  }
}

function statusCheck (promise, v, resolve, reject) {
  // 回调返回的是promise对象自身
  if (promise === v) {
    return reject(new TypeError('错误的循环调用了MyPromise自身'));
  }
  // 成功中返回的是一个MyPromise对象，执行then方法，根据状态状将新MyPromise的回调参数向下传递
  if (v instanceof MyPromise) { 
    return v.then(resolve, reject)
  }
  resolve(v);
}


// const promise  = () => new MyPromise((resolve, reject) => {
//   resolve('成功');
//   // reject('失败');
// })

// const a = () => setTimeout(() => console.log(100), 2000)

// promise().finally(() => {
//   a()
// }).then(res => console.log(res))


// let p1 = promise.then(res => {
//   console.log(res)
//   return 11
// }, err=>console.log(err))

// p1.then(res => {
//   console.log(res)
// }, reason => {
//   console.log(reason)
// })
// MyPromise.all(['1', promise()]).then(res => console.log(res))
// MyPromise.resolve('1').then(res => console.log(res))