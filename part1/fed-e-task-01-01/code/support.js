class Maybe {
  constructor (value) {
    this._value = value;
  }
  static of (value) {
    return new Maybe(value);
  }
  isNothing () {
    return this._value === null || this._value === undefined;
  }
  map (fn) {
    return this.isNothing() ? this : Maybe.of(fn(this._value))
  }
}

class Container {
  constructor (value) {
    this._value = value;
  }
  static of (value) {
    return new Container(value);
  }
  map (fn) {
    return Container.of(fn(this._value))
  }
}

module.exports = { Maybe, Container }