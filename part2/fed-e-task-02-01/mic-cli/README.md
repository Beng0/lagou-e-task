#### mic-cli
> 该脚手架工具主要通过命令行在当前命令执行的目录位置创建一个包含jsx和css文件的react组件目录

+ 首先通过`npm init`创建`package.json`文件，通过`bin`字段设置运行命令后执行的文件
+ bin目录下为脚手架工具主要执行任务文件；template、tools用于存储bin中使用到的模版和工具方法
+ 脚手架主要使用了`inquirer`进行交互选项；通过`process.cwd`获取执行命令的相对路径；通过`fs.stat`判断文件夹是否存在，`fs.mkdir`创建文件夹，`fs.writeFile`异步创建文件
