const jsx = (name) => `
import React from 'react';
import './${name}.css';

export default () => {
  return <div className='${name}_wrap'></div>
}
`;
module.exports = { jsx }