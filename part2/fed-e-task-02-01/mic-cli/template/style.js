const style = (name) => `
div.${name}_wrap {
  margin: 10px;
}
`
module.exports = { style }