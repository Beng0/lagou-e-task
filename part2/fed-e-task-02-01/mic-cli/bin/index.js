#!/usr/bin/env node

const inquirer = require('inquirer');
const path = require('path');
const fs = require('fs');
const { jsx } = require('../template/template');
const { style } = require('../template/style');
const { strFormat } = require('../tools/format');

inquirer.prompt([
  {
    type: 'input',
    name: 'name',
    message: '请输入组件名称'
  }
]).then(answers => {
  // 交互信息
  const { name } = answers;
  // 获取执行命令的相对路径
  const cwd = process.cwd();
  // 判断当前路径下是否存在以输入字符命名的文件目录
  fs.stat(path.join(cwd, strFormat(name)), (err, stats) => {
    if (err) throw err;
    // 不存在指定目录则创建
    if (!stats.isDirectory()) fs.mkdir(path.join(cwd, strFormat(name)));
    // 在指定目录下写入文件；文件内容通过方法创建，便于接受参数
    fs.writeFile(path.join(cwd, `${strFormat(name)}/${name}.jsx`), jsx(name), (err) => {
      if (err) throw err;
      console.log('jsx创建完成');
    })
    fs.writeFile(path.join(cwd, `${strFormat(name)}/${name}.css`), style(name), (err) => {
      if (err) throw err;
      console.log('css创建完成');
    })
  })
})