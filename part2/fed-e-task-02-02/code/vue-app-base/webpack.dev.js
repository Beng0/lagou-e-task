const base = require("./webpack.common");
const webpack = require("webpack");
const { merge } = require("webpack-merge");

module.exports = merge([
  base,
  {
    mode: "development",
    plugins: [
      new webpack.DefinePlugin({
        "process.env.NODE_ENV": JSON.stringify(process.env.NODE_ENV),
      }),
    ],
    devServer: {
      port: 3000,
      inline: true,
    },
  },
]);
